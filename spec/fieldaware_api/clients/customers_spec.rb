require 'spec_helper'

RSpec.describe FieldawareApi::Client::Customers do
  before do
    @client = FieldawareApi::Client.new(:api_key => api_key)
  end

  describe '#customers' do
    it 'should contains items per page' do
      VCR.use_cassette('customers') do
        per_page = 3
        items = @client.customers(page: 0, pageSize: per_page)
        expect(items).to be_a(Array)
        expect(items.size).to eq(per_page)
      end
    end
    it 'should contains items from start date with pages' do
      VCR.use_cassette('customers-date-pages') do
        dt = Time.new(2015, 12, 28)
        params = {:start_date => dt, :page => 0}
        items = []
        2.times do
          items += @client.customers(params)
          params[:page] += 1
        end
        expect(items.size).to eq(18)
      end
    end

    it 'should return nil if the date is tomorrow' do
      items = @client.customers({:start_date => Date.today + 1, :page => 0})
      expect(items).to be_nil
    end

    it 'from start date' do
      VCR.use_cassette('customers_start') do
        dt = Time.new(2016, 03, 01)
        result = @client.customers(:start_date => dt)
        expect(result.size).to eq(10)
      end
    end
    it 'from start to end date' do
      VCR.use_cassette('customers_start_end') do
        dt = Time.new(2016, 03, 01)
        result = @client.customers(:start_date => dt, :end_date => dt)
        expect(result.size).to eq(2)
      end
    end
  end

  describe '#customer' do
    context 'found' do
      before do
        VCR.use_cassette('customer') do
          @uuid = 'd08c694b9a9d4327b27c5219178ca65e'
          @customer = @client.customer(@uuid)
        end
      end
      it 'should be hash with the same uuid' do
        expect(@customer).to be_a(Hash)
        expect(@customer.uuid).to eq(@uuid)
      end
      it 'should contain contacts' do
        expect(@customer).to be_a(Hash)
        expect(@customer.contacts).to be_a(Array)
        expect(@customer.contacts.size).to eq(1)
        expect(@customer.contacts[0].phone).to eq('626 -922-7248')
      end
      it 'should contain locations' do
        expect(@customer).to be_a(Hash)
        expect(@customer.locations).to be_a(Array)
        expect(@customer.locations.size).to eq(2)
        expect(@customer.locations[0].postcode).to eq('91740')
        expect(@customer.locations[0].locality).to eq('Glendora')
      end
    end
    context 'not found' do 
      it 'should return an empty customer' do
        VCR.use_cassette('customer-not-found') do
          result = @client.customer(0)
          expect(result).to be_a(Hash)
          expect(result.id).to be_nil
        end
      end
    end
  end

end
