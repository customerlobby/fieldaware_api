require 'spec_helper'

RSpec.describe FieldawareApi::Client::CustomFields do
  before do
    @client = FieldawareApi::Client.new(:api_key => api_key)
  end

  describe '#custom_field' do

    it 'should retrieve custom field data' do
      VCR.use_cassette('custom_field') do
        id = 'decb1fea245746f0aef5ad8390a61929'
        data = @client.custom_field(id)
        expect(data).to be_a(Hash)
        expect(data.name).to eq('Startup Date')
        expect(data.type).to eq('Text')
        expect(data.position).to eq(6)
        expect(data.defaultValue).to eq('')
      end
    end

  end

end
