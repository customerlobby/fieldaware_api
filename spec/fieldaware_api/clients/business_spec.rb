require 'spec_helper'

RSpec.describe FieldawareApi::Client::Business do
  before do
    @client = FieldawareApi::Client.new(:api_key => api_key)
  end

  describe '#business' do

    it 'should retrieve business data' do
      VCR.use_cassette('business') do
        data = @client.business
        expect(data).to be_a(Hash)
        expect(data.email).to eq('amanda@canyonair.com')
      end
    end

  end

end
