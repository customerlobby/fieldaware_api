require 'spec_helper'

RSpec.describe FieldawareApi::Client do

  it 'should connect using the configured endpoint and api version' do
    client = FieldawareApi::Client.new
    endpoint = URI.parse("#{client.endpoint}#{client.api_version}/")
    connection = client.send(:connection).build_url(nil).to_s
    expect(connection).to eq(endpoint.to_s)
  end

  it 'should raise ConnectionError exception on timeout' do
    client = FieldawareApi::Client.new
    WebMock.stub_request(:get, 'https://api.fieldaware.net/').to_timeout
    expect{client.get('/')}.to raise_exception(FieldawareApi::ConnectionError)
  end

  it 'should raise ConnectionError exception if connection failed' do
    client = FieldawareApi::Client.new
    WebMock.stub_request(:get, 'https://api.fieldaware.net/').to_raise(Faraday::Error::ConnectionFailed)
    expect{client.get('/')}.to raise_exception(FieldawareApi::ConnectionError)
  end

end
