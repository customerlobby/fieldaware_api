require 'faraday'
require 'faraday_middleware'
require 'active_support/all'
require 'fieldaware_api/version'

require File.expand_path('../fieldaware_api/error', __FILE__)
Dir[File.expand_path('../../faraday/*.rb', __FILE__)].each{|f| require f}
require File.expand_path('../fieldaware_api/configuration', __FILE__)
require File.expand_path('../fieldaware_api/api', __FILE__)
require File.expand_path('../fieldaware_api/client', __FILE__)

module FieldawareApi

  extend Configuration
  # Alias for FieldawareApi::Client.new
  # @return [FieldawareApi::Client]
  def self.client(options = {})
    FieldawareApi::Client.new(options)
  end

  # Delegate to FieldawareApi::Client
  def self.method_missing(method, *args, &block)
    return super unless client.respond_to?(method)
    client.send(method, *args, &block)
  end
end
