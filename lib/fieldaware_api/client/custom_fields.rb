module FieldawareApi
  class Client
    module CustomFields

      def custom_field(id, params = {})
        get("settings/customfields/#{id}", params)
      end

    end
  end
end
