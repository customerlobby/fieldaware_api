module FieldawareApi
  class Client
    module Customers

      def customers(params = {})
        uuids = get_uuids("customer/", "Customer", params)
        uuids.map{|c|customer(c)} if uuids
      end

      def contacts(id)
        uuids = get_uuids("customer/#{id}/contact/")
        uuids.map{|c|contact(c)} if uuids
      end

      def contact(id)
        get("contact/#{id}")
      end

      def locations(id)
        uuids = get_uuids("customer/#{id}/location/")
        uuids.map{|c|location(c)} if uuids
      end

      def location(id)
        get("location/#{id}")
      end

      def customer(id, params = {})
        res = get("customer/#{id}", params)
        if res[:uuid]
          res[:contacts] = contacts(id) || []
          res[:locations] = locations(id) || []
        end
        res
      end

    end
  end
end
