module FieldawareApi
  class Client
    module Business

      def business(params = {})
        get("business", params)
      end

    end
  end
end
