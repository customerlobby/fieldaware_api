module FieldawareApi
  # Defines HTTP request methods
  module Request
    # Perform an HTTP GET request
    def get(path, options = {})
      request(:get, path, options)
    end

    # Perform an HTTP POST request
    def post(path, options = {})
      request(:post, path, options)
    end

    class GetUuidStrategy
      def get_next()
        raise "abstract method called"
      end
    end

    class PagesGetUuidsStrategy < GetUuidStrategy
      def initialize(path, params)
        @path = path
        @options = params
        @options[:page] = params[:page].to_i
      end

      def get_next()
        response = yield(@path, @options)
        @options[:page] += 1
        response.items.map(&:uuid) if response.items.size > 0
      end
    end

    class DatePagesGetUuidsStrategy < GetUuidStrategy
      def initialize(klass, page, start_date)
        @klass = klass
        @page = page
        @start_date = start_date
      end

      def get_next()
        date = @start_date + @page
        return if date > Date.today
        response = yield("changes/previous/#{date.strftime('%Y-%m-%d')}/")
        @page += 1
        response.items.map{|c| c.entity.uuid if c.entityClass == @klass}.compact
      end
    end

    class DatesGetUuidsStrategy < GetUuidStrategy
      def initialize(klass, params)
        @klass = klass
        @last = "previous/#{params[:end_date].strftime('%Y-%m-%d')}/" rescue "latest/"
        @start = params[:start_date].strftime('%Y-%m-%d') rescue ""
        @is_end = false
      end

      def get_next()
        return if @is_end
        response = yield("changes/#{@last}")
        @is_end = not(response.prev =~ %r{changes/(.*)} and $1[-11..-2] >= @start)
        @last = $1
        response.items.map{|c| c.entity.uuid if c.entityClass == @klass}.compact
      end
    end

    # Perform HTTP GETs for entity
    def get_uuids(path, klass = nil, params = {})
      result = nil
      start_date = Date.parse(params[:start_date].to_s) rescue nil
      strategy = if start_date and params[:page]
        DatePagesGetUuidsStrategy.new(klass, params[:page].to_i, start_date)
      elsif start_date or params[:end_date]
        DatesGetUuidsStrategy.new(klass, params)
      else
        PagesGetUuidsStrategy.new(path, params)
      end
      while r = strategy.get_next { |path, options|
        response = get(path, options)
        raise "invalid response" unless response["items"]
        response
      } do
        result ||= Array.new
        result += r
        break if params[:page]
      end
      result.uniq if result
    end

    private

    # Perform an HTTP request
    def request(method, path, options)
      response = connection.send(method) do |request|
        request.headers['Accept-Type'] = 'application/json'
        case method
        when :get
          formatted_options = format_options(options)
          request.url(path, formatted_options)
        when :post, :put
          request.headers['Content-Type'] = 'application/json'
          request.body = options.to_json unless options.empty?
          request.url(path)
        end
      end

      Response.create(response.body)
    end

    # Format the Options before you send them off to FieldawareApi
    def format_options(options)
      return if options.blank?
      #options[:fields]     = format_fields(options[:fields]) if options.has_key?(:fields)

      return options
    end

    # Format the fields to a format that the FieldawareApi likes
    # @param [Array or String] fields can be specified as an Array or String
    # @return String
    def format_fields(fields)
      if fields.instance_of?(Array)
        return fields.join(",")
      elsif fields.instance_of?(String)
        return fields
      end
    end

  end
end
