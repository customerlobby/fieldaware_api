module FieldawareApi
  # Wrapper for the FieldawareApi REST API.
  class Client < API
    Dir[File.expand_path('../client/*.rb', __FILE__)].each{|f| require f}

    include FieldawareApi::Client::Business
    include FieldawareApi::Client::CustomFields
    include FieldawareApi::Client::Customers
    include FieldawareApi::Client::Invoices
  end
end
