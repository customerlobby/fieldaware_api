module FieldawareApi
  class Error < StandardError; end
  class ConnectionError < Error; end
end
