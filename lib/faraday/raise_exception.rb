module FaradayMiddleware
  class RaiseException < Faraday::Middleware

    def initialize(app)
      super(app)
    end

    def call(env)
      begin
        @app.call(env)
      rescue Faraday::Error::ConnectionFailed, Faraday::TimeoutError => e
        raise FieldawareApi::ConnectionError.new(e)
      end
    end

  end
end
