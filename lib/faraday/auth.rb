module FaradayMiddleware
  class FieldawareApiAuth < Faraday::Middleware

    def initialize(app, api_key)
      @app          = app
      @api_key      = api_key
    end

    def call(env)
      env[:request_headers].merge!('Authorization' => "Token #{@api_key}")
      @app.call env
    end

  end
end
